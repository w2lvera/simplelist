#pragma once
#include "string"
#include "fstream"
using namespace std;
class Znak{
private:
    string name;
    string znskz;
    int dr[3];
    void copy(int* a);
    void copy(const int* a);
	int sum();

public:
    Znak(string name ,string b, int[3]);
    Znak();
    Znak(const Znak& a);
    friend istream& operator >> (istream& is, Znak& a);
    friend ostream& operator << (ostream& os, const Znak& a);
    ~Znak();
    Znak& operator = (const Znak& a);
    void setName(const string& a);
    string getName();
    string getZnakz();
    bool operator > (Znak& a);
    bool operator < (Znak& a);
    bool operator == (Znak& a);
     bool operator != (Znak& a);
    bool operator == (const string& a);
    void writeBin(fstream& file);
    void readBin(fstream& file);
};
