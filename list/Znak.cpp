#include "Znak.h"


Znak::Znak()
{
    name="";
    znskz="";
    dr[0]=0;
    dr[1]=0;
    dr[2]=0;
}
int Znak::sum(){
    return dr[0]*1000 + dr[1]*100 + dr[0];
}
void Znak::copy(int* a){
    for (int i=0;i<3;i++)
        dr[i]=a[i];
}
void Znak::copy(const int* a){
    for(int i=0; i<3; i++)
        dr[i]=a[i];
}
void Znak::setName(const string& a)
{
    name = a;
}

Znak::Znak(const Znak& a) {
    name = a.name;
    this->znskz = a.znskz;
    copy(a.dr);
}

Znak::Znak(string name,string b, int*d){
    this->name=name;
    this->znskz=b;
    copy(d);
}
Znak::~Znak()
{

}
string Znak::getName()
{
        return name;
}
string Znak::getZnakz()
{
    return znskz;
}

void Znak::writeBin(fstream& file)
{
    file.write((char*)this, sizeof(*this));
}
void Znak::readBin(fstream& file)
{
    file.read((char*)this, sizeof(*this));
}
ostream &operator<<(ostream& os, const Znak& a) {
    return os << a.name << "\t" << a.znskz << "\t " << a.dr[0]<<" "<<a.dr[1]<<" "<<a.dr[2];
}
istream& operator >>(istream& is, Znak& a)
{
    return is >> a.name >> a.znskz >> a.dr[0]>>a.dr[1]>>a.dr[2];
}

Znak& Znak::operator=(const Znak& a)
{
    name = a.name;
    znskz = a.znskz;
    dr[0]=a.dr[0];
    dr[1]=a.dr[1];
    dr[2]=a.dr[2];

    return *this;
}

bool Znak::operator>(Znak& a)
{
    return sum() > a.sum();
}

bool Znak::operator<(Znak& a)
{
    return sum() < a.sum();
}
bool Znak::operator==(Znak& a)
{
    return sum() == a.sum();
}
bool Znak::operator!=(Znak& a)
{
    return sum() != a.sum();
}
bool Znak::operator==(const string& a)
{
    return this->znskz == a;
}
