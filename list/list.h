#ifndef LIST_H
#define LIST_H

#endif // LIST_H
#include <string>
#include <fstream>
#include "Znak.h"
using namespace std;
struct node{
    Znak item;
    node* next;
    node(Znak k,node* n){
        item = k;
        next = n;
    }
    node(){

        next = NULL;
    }
};
class List
{
private:
    node* head;

public:
    List();
    List(Znak * a,int);
    List(const List& l);
    virtual ~List();
    void add(Znak a);
    void del(node*);
    node* findNode(Znak a);
    void delAll();
    friend ostream& operator<<(ostream& os, List& l);
    friend istream& operator>>(istream& os, List& l);
};
