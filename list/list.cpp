
#include "List.h"
#include <iostream>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

List::List()
{
    head = NULL;
}
List::List(Znak* a,int n)
{
    head = NULL;
    node * temp;
    node * ptr;
    for(int i=0;i<n;i++)
    {
        temp = new node(a[i],NULL);
        if( head == NULL )
            head=ptr=temp;
        else
            ptr ->next = temp;

        ptr = temp;
    }
}
List::List(const List& l){
           head = NULL;
           node * temp;
           node * ptr;
           for(node* i=l.head;i;i=i->next)
           {

              temp = new node(i->item,NULL);

               if( head == NULL )
                   head=ptr = temp;
               else
                   ptr ->next = temp;
               ptr = temp;
           }


}
List::~List()
{
    while(head)	del(head);

}
void List::delAll(){
    while(head)	del(head);
}

void List::add(Znak a)
{
    node * temp =  new node(a,NULL);
    if (head == NULL)
        head = temp;
    else
    {
        node* i;
        for(i = head;i->next; i=i->next );
        i->next = temp;
     }
}
void List::del (node* ptr)
{
    if(ptr)
    {
        if(ptr == head)
        {
            head = head->next;
            delete ptr;
        }
        else
        {
            node* i;
            for(i = head;i->next != ptr && i->next;i=i->next);
            i->next = ptr->next;
            delete ptr;
        }
    }
}




node * List::findNode(Znak a)
{
    node* i;
    for( i = head; i && i->item != a ; i=i->next);
    return i;
}

ostream& operator<<(ostream& os, List& l){
    if (l.head == NULL) os<<"list is empty \n";
    for(node* i = l.head;i;i=i->next)
        os<<i->item<<" \n ";
    return os;
}
istream& operator>>(istream& is, List& l){
    if(l.head!=NULL)l.delAll();
    l.head = NULL;
    node * temp;
    node * ptr;
    Znak k;
    while(is>>k){
        temp = new node(k,NULL);
        if(!l.head) l.head=ptr=temp;
        else ptr->next = temp;
        ptr = temp;
    }
    if( is.bad()) cout<<"bad" ;

    return is;
}
